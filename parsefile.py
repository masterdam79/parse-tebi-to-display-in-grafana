#!/usr/bin/env python3

from csv import reader
import configparser
from influxdb import InfluxDBClient
import json
import argparse
import datetime
import sys
import hashlib

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--file', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    csv_file = args.file
    influx_host = config['DETAILS']['INFLUXDB_URL']
    db_name = config['DETAILS']['DB_NAME']
    measurement = config['DETAILS']['MEASUREMENT']

client = InfluxDBClient(host=influx_host, port=8086)
client.create_database(db_name)
client.get_list_database()
client.switch_database(db_name)


# open file in read mode
with open(csv_file, 'r', encoding='latin-1') as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Check if csv has header and skip parsing that header is not empty
    header = next(csv_reader)
    if header != None:
        # Iterate over each row in the csv using reader object
        for i, row in enumerate(csv_reader):
            # row variable is a list that represents a row in csv
            formatted_date = datetime.datetime.strptime(row[8], '%d-%m-%y, %H:%M').strftime('%Y-%m-%d')
            formatted_time = datetime.datetime.strptime(row[8], '%d-%m-%y, %H:%M').strftime('%H:%M:%S')
            hashed_i = hashlib.sha1(str(i).encode()).hexdigest()[:2]
            formatted_time += str(int(hashed_i, 16) % 60).zfill(2)
            print(formatted_time)
            # print(row[4], float(row[6].replace(',','.')), row[8], row[9])

            json_body = [
                {
                    "measurement": measurement,
                    "tags": {
                        "personeel": row[21],
                        "bonnummer": row[7],
                        "product": row[9],
                        "variant": row[6],
                        "eenheid": row[8],
                        "aantal": row[11],
                        "sku": row[5],
                        "barcode": row[3]
                    },
                    "time": formatted_date + "T" + formatted_time + "Z",
                    "fields": {
                            "nettoprijs": float(row[13]),
                            "btwprijs": float(row[14]),
                            "korting": float(row[18]),
                            "uiteindelijkeprijs": round(float(row[13]) + float(row[14]), 2),
                            "kostprijs": float(row[20])
                        }
                }
            ]
            jsonStr = json.dumps(json_body)
            print(jsonStr)
            client.write_points(json_body)
